
# Maven settings.xml

The ti&m specific Maven settings.xml cannot be used. Please setup maven 
and your IDE to use the settings.xml file available in the docker-academy-app
root folder.

# Configuration Env variables

## springboot-frontend

| Environment Variable | Description              | Default                       |
|:---------------------|:-------------------------|:------------------------------|
| WELCOME_SVC_URL      | welcome REST service URL | http://localhost:8081/welcome |
| SERVER_PORT          | Listening port webapp    | 8080                          |
| DB_HOST              | The IP address of the DB | 10.0.0.100                    |


## springboot-welcome-svc

| Environment Variable 	| Description              	| Default                       	|
|----------------------	|--------------------------	|-------------------------------	|
| SERVER_PORT         	| Listening port webapp   	| 8081                            	|

# Database

CREATE TABLE message
(
    id BIGINT PRIMARY KEY NOT NULL,
    message VARCHAR(255)
);

INSERT INTO public.message (id, message) VALUES (1, 'Message 1 in the Database');
INSERT INTO public.message (id, message) VALUES (2, 'Message 2 in the Database');